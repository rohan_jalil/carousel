document.getElementById('arrow_right').onclick = function(){
  document.getElementById('arrow_right').style.pointerEvents = 'none';
  var image_div = document.getElementById("images");
  var child = image_div.children[0];
  var lastNode = image_div.children[2];            
  moveRight(image_div.children[0],image_div,child,lastNode);
}

document.getElementById('arrow_left').onclick = function(){
  document.getElementById('arrow_left').style.pointerEvents = 'none';
  var image_div = document.getElementById("images");
  var child = image_div.children[2];
  var firstNode = image_div.children[0];
  var node = image_div.insertBefore(child,firstNode);
  node.removeAttribute("style");
  move(node);
};

function move(elem) {

  var marginLeft = -1200;

  function frame() {
    marginLeft = marginLeft+5;   // update parameters
    elem.style.marginLeft = marginLeft + 'px' // show frame
    if (marginLeft == 0){  // check finish condition
      clearInterval(id);
      document.getElementById('arrow_left').style.pointerEvents = 'visible';
      
    }
  }
  var id = setInterval(frame, 1) // draw every 1ms
}

function moveRight(elem,image_div,child,lastNode) {

  var marginLeft = 0;

  function frame() {
    marginLeft = marginLeft-5;   // update parameters
    elem.style.marginLeft = marginLeft + 'px' // show frame
    if (marginLeft == -1200){  // check finish condition
      clearInterval(id);
      image_div.appendChild(child,lastNode);
      elem.removeAttribute("style");
      document.getElementById('arrow_right').style.pointerEvents = 'visible';

    }
  }

  var id = setInterval(frame, 1) // draw every 1ms
}

function generateClick(){
  document.getElementById('arrow_right').click()
}

setInterval(generateClick, 5000);